import os
import re
import sys
import time
import copy
import Image
import shutil
import commands
import settings
import datetime
import unidecode
import traceback
import subprocess

from hashlib                 import sha1
from tagging.models          import Tag
from django.core.files       import File
from tajhiz.saisie.models    import *
from django.contrib.gis.gdal import DataSource
from django.contrib.gis.geos import LineString
from django.contrib.gis.geos import MultiLineString

GPX_LAYERS_WAYPOINTS     = 0
GPX_LAYERS_ROUTES        = 1
GPX_LAYERS_TRACKS        = 2
GPX_LAYERS_ROUTES_POINTS = 3
GPX_LAYERS_TRACKS_POINTS = 4

hour = datetime.timedelta(hours=1)

def git_hash(data):
	s = sha1()
	s.update("blob %u\0" % len(data))
	s.update(data)
	return s.hexdigest()

def git_hash_file(filepath):
	file_hash = None
	file = open(filepath, 'rb')
	try:
		file_hash = git_hash(file.read())
	finally:
		file.close()
	return file_hash


def image_name_to_timestamp_renaming(image_path):
	'''
		This function convert the image files to timestamp format

		@type  image_path: string
		@param image_path: the absolute path of the image files folder
		@return: None
	'''
	image_media_re = re.compile('(\d{4}-\d{2}-\d{2} \d{2}\.\d{2}\.\d{2})\.jpg', re.IGNORECASE)
	for root, dirs, files in os.walk(image_path):
		for file in files:
			if image_media_re.search(file):
				new_file = "%d.jpg" % int(time.mktime(datetime.datetime.strptime(file, "%Y-%m-%d %H.%M.%S.jpg").timetuple()))
				os.rename(os.path.join(image_path, file), os.path.join(image_path, new_file))

def audio_image_mapping(audio_root, image_root):
	'''
		This function map audio and image file based on the timestamp of each one (contained in the file name).
		to achieve this goal the function scan the audio root and image root for images and audio files, once
		this done, it sort the files by timestamp and try to map each media to the other.

		If for some reason the mapping failes, we assume the audio file to be correct and replace the image file
		with not_found string. (this have to be corrected somewhere else)

		@type  audio_root: string
		@param audio_root: absolute path of the audio files
		@type  image_root: string
		@param image_root: absolute path of the image files
		@return: list of list of maped media ((audio_timestamp, image_timestamp),)
	'''
	audio_image_map    = []
	audios_timesstamps = []
	images_timesstamps = []
	image_media_re     = re.compile('(.*)\.jpg', re.IGNORECASE)
	audio_media_re     = re.compile('(.*)\.mp3', re.IGNORECASE)
	for root, dirs, files in os.walk(image_root):
		for file in files:
			image_id = image_media_re.search(file)
			if image_id:
				image_timestamp = image_id.group(1)
				images_timesstamps.append(image_timestamp)
	for root, dirs, files in os.walk(audio_root):
		for file in files:
			audio_id = audio_media_re.search(file)
			if audio_id:
				audio_timestamp = audio_id.group(1)
				audios_timesstamps.append(audio_timestamp)

	def map_func(elem_1, elem_2, switch_position):
		affected = []
		iteration_nb = 0
		if len(elem_1) > 1:
			last_elem_2_found_idx = 0 if not switch_position else len(elem_2)-1
			elem_1_range = range(0, len(elem_1))
			if switch_position:
				elem_1_range.reverse()
			for elem_1_idx in elem_1_range:
				found_idx = None
				elem_2_range = range(last_elem_2_found_idx, len(elem_2))
				if switch_position:
					elem_2_range.reverse()
				for elem_2_idx in elem_2_range:
					if not switch_position:
						cond = elem_1[elem_1_idx] <= elem_2[elem_2_idx] and elem_1_idx+1 <= len(elem_1)-1 and elem_2[elem_2_idx] < elem_1[elem_1_idx+1]
					else:
						cond = elem_1[elem_1_idx] >= elem_2[elem_2_idx] and elem_1_idx-1 >= 0             and elem_2[elem_2_idx] < elem_1[elem_1_idx-1]
					if cond :
						found_idx = elem_2_idx
					else:
						break
				if found_idx != None :
					if (found_idx - last_elem_2_found_idx) > 1:
						for not_found_idx in range(last_elem_2_found_idx, found_idx-1):
							if not switch_position: audio_image_map.append([elem_1[not_found_idx], 'not_found'] )
							else                  : audio_image_map.append(['not_found',  elem_1[not_found_idx]])
					if not switch_position: audio_image_map.append([elem_1[elem_1_idx], elem_2[found_idx]])
					else                  : audio_image_map.append([elem_2[found_idx], elem_1[elem_1_idx]])
					affected.append(elem_2[found_idx])
					last_elem_2_found_idx = found_idx + 1 if not switch_position else found_idx - 1

				elif iteration_nb == 0 and elem_1[elem_1_range[0]] < elem_2[elem_2_range[0]]:
						if not switch_position: audio_image_map.append([elem_1[elem_1_range[0]], elem_2[elem_2_range[0]]])
						else                  : audio_image_map.append([elem_2[elem_2_range[0]], elem_1[elem_1_range[0]]])
						affected.append(elem_2[elem_2_range[0]])
						last_elem_2_found_idx = 1 if not switch_position else len(elem_2) - 2
				elif elem_1_idx == len(elem_1) - 1 and last_elem_2_found_idx == len(elem_2) - 1:
					if not switch_position:
						cond = elem_1[elem_1_idx] <= elem_2[elem_2_idx]
					else:
						cond = elem_1[elem_1_idx] >= elem_2[elem_2_idx]
					if cond :
						if not switch_position: audio_image_map.append([elem_1[elem_1_idx], elem_2[elem_2_idx]])
						else                  : audio_image_map.append([elem_2[elem_2_idx], elem_1[elem_1_idx]])
						affected.append(elem_2[elem_2_idx])
						last_elem_2_found_idx = 0 if not switch_position else len(elem_2) - 1
				else:
					if not switch_position: audio_image_map.append([elem_1[elem_1_idx], 'not_found'] )
					else                  : audio_image_map.append(['not_found',  elem_1[elem_1_idx]])
				iteration_nb += 1
		else:
			if len(elem_2) == 1:
				if not switch_position: audio_image_map.append([elem_1[0], elem_2[0]])
				else                  : audio_image_map.append([elem_2[0], elem_1[0]])
			else:
				if not switch_position: audio_image_map.append([elem_1[0]  , 'not_found'])
				else                  : audio_image_map.append(['not_found', elem_1[0]  ])

	audios_timesstamps.sort()
	images_timesstamps.sort()

	if len(audios_timesstamps) >= len(images_timesstamps):
		map_func(audios_timesstamps, images_timesstamps, False)
	else:
		audios_timesstamps.reverse()
		images_timesstamps.reverse()
		map_func(images_timesstamps, audios_timesstamps, True )

	return audio_image_map

def slugify(data):
	'''
		This function slugify a string (custom slug format specification)

		@type  data: string
		@param data: string to be slugified
		@return : slugified version of the string
	'''
	slug = unidecode.unidecode(data)
	slug = slug.encode('ascii', 'ignore').lower()
	slug = re.sub(r'[^a-z0-9]+', '-', slug).strip('-')
	slug = re.sub(r'[-]+', '-', slug)
	return slug

def test_folder_structure(current_dir, camera_dir, depth_level=1, max_depth=2):
	'''
		This function inspects, verify and try to correct some anomality in the data file structre.
		To do so the function searchs the audio and track folders starting from the camera folder, if
		no directory with valid names is found in the current depth level we go one level up. (the
		maximum depth we could be going is specified by the max_depth argument)

		@type  current_dir: string
		@param current_dir: the current base path searched
		@type  camera_dir: string
		@param camera_dir: the camera directory used as reference
		@type  depth_level: int
		@param depth_level: the current depth level
		@type  max_depth: int
		@param max_depth: the maximum level depth we can go up
		@return: dictionary with extensive directories information { 'status', 'gpx_dir', 'road_dir', 'road_name', 'audio_dir', 'camera_dir', 'road_date', 'dr_dir_name'}
	'''
	road_date_re = re.compile('(.*)[-_]{1}(\d{2}[_-]{1}\d{2}[_-]{1}\d{4})')
	road_dir     = os.path.dirname(os.path.abspath(current_dir                 ))
	dr_dir       = os.path.dirname(os.path.dirname(os.path.abspath(current_dir)))
	dr_dir_name  = os.path.split(dr_dir  )[1]
	road_name    = os.path.split(road_dir)[1]
	road_data    = road_date_re.search(road_name)
	if not road_data:
		if depth_level < max_depth:
			return test_folder_structure(os.path.dirname(current_dir), camera_dir, depth_level+1)
		else:
			return {
				'status'      : False      ,
				'road_dir'    : road_dir   ,
				'road_name'   : road_name  ,
				'dr_dir_name' : dr_dir_name,}
	road_id   = road_data.group(1) 
	gpx_dir   = os.path.abspath(os.path.join(road_dir, './tracks'))
	audio_dir = os.path.abspath(os.path.join(road_dir, './audio-records'))
	return {
		'status'      : True       , 
		'gpx_dir'     : gpx_dir    ,
		'road_dir'    : road_dir   ,
		'road_name'   : road_id    ,
		'audio_dir'   : audio_dir  ,
		'camera_dir'  : camera_dir ,
		'road_date'   : road_data.group(2),
		'dr_dir_name' : dr_dir_name,}

def slugify_folder(folder_path):
	'''
		This function convert folders and files name to a slugified version
		recursively starting from the folder_path path

		@type  folder_path: string
		@param folder_path: base absolute path to start form
		@return: None
	'''
	pending_operation = []
	pr_dir   = os.path.split(os.path.abspath(folder_path))[0]
	dr_dir   = os.path.split(os.path.abspath(folder_path))[1]
	slug_dir = slugify(dr_dir)
	new_path = os.path.join(pr_dir, slug_dir)
	if slug_dir != dr_dir:
		print "renaming %s to %s" % (os.path.join(pr_dir, dr_dir), new_path)
		shutil.move(os.path.join(pr_dir, dr_dir), new_path)
	for sub_object in os.listdir(new_path):
		if os.path.isdir(os.path.abspath(os.path.join(new_path, sub_object))):
			slugify_folder(os.path.abspath(os.path.join(new_path,sub_object)))

def explore_data(base_dir):
	results  = []
	failures = []
	rs_test  = []
	###############################################
	# Verify the base folder existence            #
	###############################################
	if not os.path.exists(os.path.abspath(base_dir)):
		raise Exception("explore_data", "sorry requested path not found %s" % os.path.abspath(base_dir))
	###############################################
	# List all directories in the first level:    #
	#   - No file is allowed in the first level   #
	#   - All fs tree object are slugified        #
	###############################################
	for i in os.listdir(base_dir):
		if os.path.isdir(os.path.join(base_dir,i)):
			slugify_folder(os.path.join(base_dir,i))
	###############################################
	# First structure check/correction:           #
	#   - audio-tracks renamed to audio-records   #
	#   - folders must not be empty               #
	###############################################
	for root, dirs, files in os.walk(base_dir):
		for dir in dirs:
			current_dir = os.path.abspath(os.path.join(root, dir))
			if dir == "audio-tracks":
				print "renaming %s to %s" % (current_dir, os.path.abspath(os.path.join(root, 'audio-records')))
				shutil.move(current_dir, os.path.abspath(os.path.join(root, 'audio-records')))
				current_dir = os.path.abspath(os.path.join(root, 'audio-records'))
			if os.listdir(current_dir) == []:
				raise Exception("explore_data", "Directory %s seems to be empty (very suspicious)!!" % current_dir)
	###############################################
	# Looking for the 'camera' folder:            #
	#   - Computing the track folder  path        #
	#   - Computing the audio-records folder path #
	#   - Computing the road path and name        #
	#   - Extracting the road date or try to fix  #
	###############################################
	for root, dirs, files in os.walk(base_dir):
		for dir in dirs:
			current_dir = os.path.abspath(os.path.join(root, dir))
			if dir == 'camera':
				rs_test = test_folder_structure(os.path.abspath(current_dir), os.path.abspath(current_dir))
				if rs_test['status'] == False:
					road_dir    = rs_test['road_dir'   ]
					road_name   = rs_test['road_name'  ]
					dr_dir_name = rs_test['dr_dir_name']
					print "Possible target %s (%s | %s )" % (road_dir, dr_dir_name, road_name)
					print "\terror applaying regular expression to folder %s " % road_name
					print "Correction attemp:"
					image_re = re.compile("(\d{4}-\d{2}-\d{2}) \d{2}\.\d{2}.\d{2}\.jpg", re.IGNORECASE)
					found_solution = False
					for i in os.listdir(current_dir):
						print "\t\ttrying with %s" % i
						image_date = image_re.search(i)
						if image_date:
							try:
								dt = datetime.datetime.strptime(i, "%Y-%m-%d %H.%M.%S.jpg")
								new_road_dir = road_dir+'-'+dt.strftime('%d-%m-%Y')
								shutil.move(road_dir, new_road_dir)
								road_id   = road_name+'-'+dt.strftime('%d-%m-%Y')
								gpx_dir   = os.path.abspath(os.path.join(new_road_dir, './tracks'))
								audio_dir = os.path.abspath(os.path.join(new_road_dir, './audio-records'))
								rs_test = dict({
									'status'    : True     ,
									'gpx_dir'   : gpx_dir  ,
									'audio_dir' : audio_dir,
									'road_name' : road_id  ,
									'road_date' : dt.strftime('%d-%m-%Y')}.items() + rs_test.items())
								found_solution = True
								break
							except Exception as e:
								raise Exception("explore_data", "time validation failed with exception %s" % e)
					if not found_solution:
						failure.appen(rs_test)
						raise Exception('explore_data', 'failed to correct road name %s under %s' % (road_name, dr_dir_name))
				else:
					results.append(rs_test)
			if dir == '.thumbnails':
				print "deleting " + current_dir
				shutil.rmtree(current_dir)
	###############################################
	# Second test of the computed paths:          #
	#   - Extracted date must be valid            #
	#   - Extracted path must not be nested       #
	#   - Extracted path must not be empty        #
	#   - Extracted path must containt valid files#
	###############################################
	to_test_paths = (
			{'path_key' : 'audio_dir' , 'path_ext' : ('.3gp', '.ogg', '.mp3')},
			{'path_key' : 'camera_dir', 'path_ext' : ('.jpg',               )},
			{'path_key' : 'gpx_dir'   , 'path_ext' : ('.gpx',               )},
		)

	for computed_path in results:
		try:
			road_date = computed_path['road_date']
			datetime.datetime.strptime( road_date , "%d-%m-%Y") 
		except ValueError:
			raise Exception("explore_data", "invalid date on: %s (%s)" % (computed_path, road_date))
		for validation_map in to_test_paths:
			current_path_to_test = computed_path[validation_map['path_key']]
			if not os.path.exists(current_path_to_test):
				raise Exception("explore_data", "folder not found %s" % current_path_to_test)
			else:
				for path_root, path_dirs, path_files in os.walk(current_path_to_test):
					if len(path_dirs) > 0:
						raise Exception("explore_data", "%s contains subfolders, the file structure is probably wrong, please check it, aborting operation" % current_path_to_test)
					elif len(path_files) == 0:
						raise Exception("explore_data", "%s is empty, aborting operations" % current_path_to_test)
					else:
						for path_file in path_files:
							current_path_file, current_path_file_ext = os.path.splitext(path_file)
							valid_ext = False
							for extension in validation_map['path_ext']:
								if current_path_file_ext == extension:
									valid_ext = True
									break
							if not valid_ext:
								raise Exception("explore_data", "found file %s on %s with invalid extension yatatalfa7 !!!" % (path_file, path_root))
		slug = slugify(computed_path['road_name'])
		slug+= '-' + road_date
		new_road_dir = os.path.abspath(os.path.join(computed_path['road_dir'], '../'+slug))
		if computed_path['road_dir'] != new_road_dir:
			shutil.move(computed_path['road_dir'], new_road_dir)
	###############################################
	# Compressing the data structure to tree like #
	#   - Testing found local_direction against db#
	###############################################
	local_directions           = []
	results_by_local_direction = []

	for result in results:
		local_directions.append(result['dr_dir_name'])
	local_directions = set(local_directions)
	found_local_directions = DirectionRegionale.objects.filter(label__in = local_directions).values('label')
	for found_local_direction in found_local_directions:
		db_found_valid_direction = False
		for local_direction in local_directions:
			if found_local_direction['label'] == local_direction:
				db_found_valid_direction = True
				break
		if not db_found_valid_direction:
			raise Exception("explore_data", "local direction %s not found in the database" % found_local_direction)
		else:
			results_by_local_direction.append({ 'local_direction' : local_direction, 'roads_data' : [] })
	for result in results:
		for local_direction_tree_idx in range (0, len(results_by_local_direction)):
			if results_by_local_direction[local_direction_tree_idx]['local_direction'] == result['dr_dir_name']:
				results_by_local_direction[local_direction_tree_idx]['roads_data'].append(
					{
						'gpx_dir'    : result['gpx_dir'   ],
						'road_dir'   : result['road_dir'  ],
						'audio_dir'  : result['audio_dir' ],
						'road_date'  : result['road_date' ],
						'road_name'  : result['road_name' ],
						'camera_dir' : result['camera_dir'],
					}
				)
	return { 'results' : results_by_local_direction, 'failures' : failures }

def audio_files_names_transformation(audio_root):
	'''
		This function tranform all the 3gp files under audio_root folder to mp3 and ogg with a reduce name.
		PS: this function relayes on the presence of the ffmpeg and mplayer conversion tools

		@type  audio_root: string
		@param audio_root: the base absolute path of the folder to be processed
		@return: None
	'''
	audio_media_re = re.compile('(\d{13})\.3gp')
	for root, dirs, files in os.walk(audio_root):
		for file in files:
			audio_id = audio_media_re.search(file)
			if audio_id:
				audio_timestamp = audio_id.group(1)
				audio_time = datetime.datetime.fromtimestamp(float(audio_timestamp)/1000)
				new_time = audio_time - hour
				old_file = "%s/%s"     % (audio_root, file)
				new_file = "%s/%d.3gp" % (audio_root, int(time.mktime(new_time.timetuple())))
				print "renaming %s to %s" % (old_file, new_file)
				os.rename(old_file, new_file)

	os.system(                                                                                                                     
		'''for i in `find %s -maxdepth 1 -name '*.3gp' -type f`
		do
		mp3_file=`echo $i | sed 's/3gp/mp3/'`
		ogg_file=`echo $i | sed 's/3gp/ogg/'`
		echo 'converting audio file' $i ' to ' $mp3_file 'and' $ogg_file
		if [ ! -e $mp3_file ]
		then
		ffmpeg -y -loglevel quiet -i $i `echo $i | sed 's/3gp/mp3/'` >/dev/null 2>/dev/null </dev/null
		fi
		if [ ! -e $ogg_file ]
		then
		mplayer -really-quiet -vc dummy -ao pcm:file=/dev/stdout $mp3_file 2> /dev/null | oggenc -q 7 -o $ogg_file -
		fi
		rm $i
		done''' % audio_root)

def load_gpx_waypoints(gpx_root):
	'''
		This function extracts all the waypoints from a gpx file, the extracted data
		also contains some extra information specific to the format used by the android
		application with this project.

		Using the base path passed in parameter, the function will search all gpx files
		matching the regular expression \d{4}-\d{2}-\d{2}(.*)_panels\.gpx (project specific).

		The data in these files is merged into a single result.

		@type  gpx_root: string
		@param gpx_root: absolute base path in which resides the gpx files
		@return: list of list representing the waypoints data ( PointObject, Direction, TimeObject, Timestamp )
	'''
	waypoints     = []
	panels_gpx_re = re.compile('(\d{4}-\d{2}-\d{2})(.*)_panels\.gpx')
	for root, dir, files in os.walk(gpx_root):
		for file in files:
			panels_id = panels_gpx_re.search(file)
			if panels_id:
				file_date             = panels_id.group(1)
				datasource            = DataSource(gpx_root+'/'+file)
				waypoints_layer       = datasource[GPX_LAYERS_WAYPOINTS]
				waytpoints_points     = waypoints_layer.get_geoms()
				waytpoints_direction  = waypoints_layer.get_fields('name')
				waytpoints_event_time = waypoints_layer.get_fields('time')
				for i in range(0, len(waytpoints_points)):
					waypoints.append((
						waytpoints_points    [i], 
						waytpoints_direction [i], 
						waytpoints_event_time[i],
					))
	for i in range(0, len(waypoints)):
		waypoints[i] += (int(time.mktime(waypoints[i][2].timetuple())),)
	return waypoints

def load_gpx_tracks(gpx_root):
	'''
		This function extracts all the tracks from a gpx file.

		Using the base path passed in parameter, the function will search all gpx files
		matching the regular expression \d{4}-\d{2}-\d{2}[_\d*]*\.gpx (project specific).

		The data in these files is merged into a single result.

		@type  gpx_root: string
		@param gpx_root: absolute base path in which resides the gpx files
		@return: list of dictionary representing the track data { tracks, total, files }
	'''
	tracks    = []
	gpx_files = ""
	tracking_gpx_re = re.compile('(\d{4}-\d{2}-\d{2})[_\d*]*\.gpx')
	for root, dir, files in os.walk(gpx_root):
		for file in files:
			track_id = tracking_gpx_re.search(file)
			if track_id:
				total      = []
				tracks_gt  = []
				file_date  = track_id.group(1)
				datasource = DataSource(gpx_root+'/'+file)
				tracks_lyr = datasource[GPX_LAYERS_TRACKS]
				tracks_geo = tracks_lyr.get_geoms()
				for track_idx in range(0, len(tracks_geo)):
					if tracks_geo[track_idx].point_count == 1:
						tracks_gt.append(MultiLineString([LineString(tracks_geo[track_idx].tuple[0] + (tracks_geo[track_idx].tuple[0][0],))]))
					else:
						tracks_gt.append(tracks_geo[track_idx].geos)
				tracks.append({'file': file, 'track': tracks_gt})
	for track in tracks:
		current_tracks = track['track']
		gpx_files += track['file']+'_'
		if len(current_tracks) > 0:
			initial_track = copy.deepcopy(current_tracks[0])
			for i in current_tracks:
				if i != initial_track:
					initial_track = initial_track.union(i)
			total.append(initial_track)
	gpx_files 
	if len(total) > 0:
		initial_track = copy.deepcopy(total[0])
		for track in total:
			if initial_track != track:
				initial_track = initial_track.union(track)
	return { 'tracks' : tracks, 'total' : initial_track, 'files' : gpx_files[:-1] }

def synchronize_files_time(audio_root, image_root):
	audio_files_id = []
	image_files_id = []
	audio_media_re = re.compile('(.*)\.mp3', re.IGNORECASE)
	image_media_re = re.compile('(.*)\.jpg', re.IGNORECASE)
	for root,dirs,files in os.walk(audio_root):
		for audio_file in files:
			audio_id = audio_media_re.search(audio_file)
			if audio_id:
				audio_files_id.append(audio_id.group(1))
	for root,dirs,files in os.walk(image_root):
		for image_file in files:
			image_id = image_media_re.search(image_file)
			if image_id:
				image_files_id.append(image_id.group(1))
	audio_files_id.sort()
	image_files_id.sort()
	try:
		image_date_sample = datetime.datetime.fromtimestamp(float(image_files_id[0]))
		audio_date_sample = datetime.datetime.fromtimestamp(float(audio_files_id[0]))
	except Exception as e:
		print "failed to parse file timestamp name (a|i) (%s|%s)[%s,%s]" % (
			audio_files_id[0], 
			image_files_id[0], 
			audio_root, 
			image_root)
		raise e
	if abs(image_date_sample - audio_date_sample).seconds > 3600:
		delta_positiv = True
		if image_date_sample < audio_date_sample:
			delta = False
		for i in image_files_id:
			if delta_positiv: new_date = int(time.mktime((datetime.datetime.fromtimestamp(float(i)) - hour).timetuple()))
			else:             new_date = int(time.mktime((datetime.datetime.fromtimestamp(float(i)) + hour).timetuple()))
			print("renaming "+os.path.join(image_root,i+'.jpg') +" to "+ os.path.join(image_root,str(new_date)+'.jpg'))
			os.rename(os.path.join(image_root,i+'.jpg'), os.path.join(image_root,str(new_date)+'.jpg'))

def import_data(RAW_DATA_DIR='/var/www/django/tajhiz/media/Raw/'):
	log = open("/tmp/douda.log", 'w')
	mandatory_programs = ( 'mplayer', 'ffmpeg', 'oggenc', 'sed')

	for program in mandatory_programs:
		if commands.getstatusoutput('which '+program)[0] != 0:
			raise Exception("audio_files_names_transformation", "required program %s not found" % program)
	try:
		mapping     = []
		exploration = explore_data(RAW_DATA_DIR)
		if len(exploration['failures']) > 0:
			raise Exception('import_data', "Some  errors where detected in the data:\n%s" % exploration['failures'])
		for local_direction_idx in exploration['results']:
			for road_data_idx in local_direction_idx['roads_data']:
				audio_files_names_transformation(road_data_idx['audio_dir'])
				image_name_to_timestamp_renaming(road_data_idx['camera_dir'])
				print "synchronizing %s and %s" % (road_data_idx['audio_dir'], road_data_idx['camera_dir'])
				synchronize_files_time(road_data_idx['audio_dir'], road_data_idx['camera_dir'])
		for local_direction_idx in exploration['results']:
			for road_data_idx in local_direction_idx['roads_data']:
				tmp_map         = []
				not_found       = []
				waypoints       = load_gpx_waypoints(road_data_idx['gpx_dir'])
				audio_image_map = audio_image_mapping(road_data_idx['audio_dir'], road_data_idx['camera_dir'])

				waypoints       = clean_duplicate_waypoints(waypoints)
				waypoints       = sorted(waypoints,       key = lambda dt: dt[3])
				audio_image_map = sorted(audio_image_map, key = lambda dt: dt[0])

				log.write("%s \n%s\n" % (road_data_idx['audio_dir'], road_data_idx['camera_dir']))
				log.write("waypoints length: %s\n"   % len(waypoints))
				log.write("audio image length: %s\n" % len(audio_image_map))

				for map_idx in range(0, len(audio_image_map)):
					audio_image_map[map_idx].append(False)

				for waypoint_idx in range(0, len(waypoints)):
					found_at           = None;
					waypoint_timestamp = waypoints[waypoint_idx][3]
					for map_idx in range(0, len(audio_image_map)):
						mapped_audio_timestamp = audio_image_map[map_idx][0] if audio_image_map[map_idx][0] != 'not_found' else audio_image_map[map_idx][1]
						if int(waypoint_timestamp) > int(mapped_audio_timestamp):
							found_at = map_idx
					if found_at != None and not audio_image_map[found_at][2]:
						log.write("image (%s | %s) (%s | %s) found at waypoint %s (%s)\n" % (
							audio_image_map[found_at][0],
							audio_image_map[found_at][1],
							datetime.datetime.fromtimestamp(float(audio_image_map[found_at][0])) if audio_image_map[found_at][0] != 'not_found' else 'not_found',
							datetime.datetime.fromtimestamp(float(audio_image_map[found_at][1])) if audio_image_map[found_at][1] != 'not_found' else 'not_found',
							waypoints[waypoint_idx][3],
							waypoints[waypoint_idx][2],
						))
						audio_image_map[found_at][2] = True
						tmp_map.append((audio_image_map[found_at], waypoints[waypoint_idx]))
					else:
						not_found.append(waypoints[waypoint_idx])
				road_data_idx['dr_dir_name'] = local_direction_idx['local_direction']
				mapping.append({'collection' : road_data_idx, 'map' : tmp_map, 'not found' : not_found })
		async_gpx    = 0
		async_media  = 0
		total_events = 0
		for i in mapping:
			async_gpx += len(i['not found'])
			for j in i['map']:
				if j[0][1] == 'not_found':
					async_media += 1
				total_events += 1
		print "total = %s | audio/image not in sync = %s | gpx points not in sync %s" % (total_events, async_media, async_gpx)
		return mapping
	except Exception as e:
		print "Sorry, operation failed: %s" % e
		traceback.print_exc(file=sys.stdout)

def galleries_creation(mapping):
	image_counter = 1
	not_found_image = File(open(os.path.join(settings.MEDIA_ROOT,'Img/not_found.jpg')))
	for mapping_element in mapping:
		tag  = 'ROAD_'+ mapping_element['collection']['road_name'  ] + " "
		tag += 'DATE_'+ mapping_element['collection']['road_date'  ] + " "
		tag += 'DR_'  + mapping_element['collection']['dr_dir_name']
		gallery_title = mapping_element['collection']['dr_dir_name'] + '_' +mapping_element['collection']['road_name'] + '_' + mapping_element['collection']['road_date']
		gallery_description = "Collection %s took the %s for the DR of %s" % (
			mapping_element['collection']['road_name'],
			mapping_element['collection']['road_date'],
			mapping_element['collection']['dr_dir_name'])

		print "creating gallery %s" % gallery_title
		road_date = datetime.datetime.strptime(mapping_element['collection']['road_date'], "%d-%m-%Y")

		gallery = Gallery.objects.create(
			tags        =tag                ,
			title       =gallery_title      ,
			is_public   =True               ,
			title_slug  =gallery_title      ,
			description =gallery_description,)
		
		GalleryUserOwnership.objects.create(gallery=gallery)

		tracks   = load_gpx_tracks(mapping_element['collection']['gpx_dir'])
		
		collecte = Collecte.objects.create(
			sens   = "undefined", 
			date   = road_date  ,
			equipe = 1          ,)
		
		Tracking.objects.create(
			gpx_file = tracks['files'], 
			collecte = collecte       ,
			parcour  = tracks['total'],)

		for event in mapping_element['map']:
			valid_image     = True
			audio_timestamp = event[0][0]
			image_timestamp = event[0][1]
			image_file = os.path.abspath(os.path.join(mapping_element['collection']['camera_dir'],image_timestamp+'.jpg'))
			try:
				image_obj=Image.open(image_file)
				image_obj.load
				image_obj.verify()
			except IOError:
				valid_image = False
				print "failed to open file %s as valid image" % image_file
				print "replacing it with not_found and marking the media as invalid_image"
			photo_title    = image_timestamp
			new_photo_file = os.path.abspath(os.path.join(settings.MEDIA_ROOT,"photologue/photos/"+photo_title+'.jpg'))
			try:
				p = Photo.objects.get(title=photo_title)
				print "photo with the same name already exists skiping (%s)" % photo_title
				continue
			except Photo.DoesNotExist:
				if valid_image:
					print "opening file %s " % new_photo_file
					photo_image = File(open(image_file, 'r'))
				else:
					print "replacing image with not found"
					photo_image = not_found_image

				photo = Photo(
					tags       = tag        ,
					title      = photo_title,
					caption    = photo_title,
					is_public  = True       ,
					title_slug = photo_title,)
				if image_timestamp != 'not_found':
					same_photos = Photo.objects.filter(title__contains=image_timestamp)
					initial_subset_hash   = None
					secondary_subset_hash = None
					if len(same_photos) > 0:
						try:
							same_photos[0].image.open()
							initial_subset_hash = git_hash(same_photos[0].image.read())
						finally:
							same_photos[0].image.close()
						secondary_subset_hash = git_hash_file(image_file)
						if initial_subset_hash == secondary_subset_hash:
							raise Exception("galleries_creation", "another photo contains a subset of this one (%s | %s) and have the same sha signature" % (photo_title, same_photos))
						else:
							photo_title = str(float(photo_title) + 1)
							print "renaming %s to %s due to pre-existing file with the same name" % (image_timestamp, photo_title)
				photo.image.save(photo_title+'.jpg', photo_image)
				gallery.photos.add(photo)
				audio_file = os.path.join(mapping_element['collection']['audio_dir'], audio_timestamp)

				media = Media.objects.create(
					image        = photo      ,
					audio        = audio_file ,
					audio_valide = True       ,
					image_valide = valid_image,)

				Event.objects.create(
					date     = road_date       ,
					sens     = event[1][1]     , 
					media    = media           ,
					collecte = collecte        ,
					position = event[1][0].geos,)

			image_counter += 1

def geo_fixtures_load():
	'''
		This function loads all the geographical fixture from shp files
	'''
	import tools.load_voie
	import tools.load_noeud
	import tools.load_direction_regionale
	tools.load_voie.run()
	tools.load_noeud.run()
	tools.load_direction_regionale.run()

def clean_duplicate_waypoints(waypoints):
	'''
		This function searchs and deletes the duplicated points from a
		list of waypoints. The comparaison criterias are the time of the
		waypoint

		@type  waypoints: list of waypoints ( PointObject, Sign, DateTime, Timestamp )
		@param waypoints: the list of waypoints to be sanitized
		@return: list of waypoints ( PointObject, Sign, DateTime, Timestamp ) with unique timestamps
	'''
	for i in range(0, len(waypoints)):
		for j in range(0, len(waypoints)):
			if i != j and waypoints[i][3] == waypoints[j][3] and i < j:
				del waypoints[j]
				return clean_duplicate_waypoints(waypoints)
	return waypoints