from saisie.models            import Noeud
from tools.import_tool        import slugify
from django.contrib.gis.utils import LayerMapping

node_mapping = {
	'type'     : 'TYPE'   ,
	'label'    : 'SEGMENT',
	'position' : 'POINT'  ,
}

tunisia_nodes = "/var/www/django/tajhiz/fixtures/shapes/nodes/nodes.shp"

def run(verbose=True):
    lm = LayerMapping(Noeud, tunisia_nodes, node_mapping, transform=True, encoding='utf8')
    lm.save(strict=True, verbose=verbose)
    for i in Noeud.objects.all():
		i.type   = slugify(i.type )
		i.label  = slugify(i.label)
		i.save()