from saisie.models            import Voie
from tools.import_tool        import slugify
from django.contrib.gis.utils import LayerMapping

voie_mapping = {
	'label'     : 'NOM',
	'nom_usage' : 'NOM_USAGE',
	'largeur'   : 'LARGEUR',
	'cycle'     : 'CYCLE',
	'type'      : 'TYPE',
	'geom'      : 'LINESTRING',
}

tunisia_voie = "/var/www/django/tajhiz/fixtures/shapes/roads/roads.shp"

def run(verbose=True):
    lm = LayerMapping(Voie, tunisia_voie, voie_mapping, transform=True, encoding='utf8')
    lm.save(strict=True, verbose=verbose)
    for i in Voie.objects.all():
		i.label     = slugify(i.label    )
		i.nom_usage = slugify(i.nom_usage)
		i.save()