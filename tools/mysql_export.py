import datetime

def export_to_mysql(mapping):
	result_file = open("/tmp/export.sql", "w")
	for i in mapping:
		for j in i['map']:
			result_file.write('''
				insert into data (
				direction, 
				road_name, 
				road_date, 
				audio_ts, 
				image_ts, 
				gpx_lon, 
				gpx_lat, 
				sign, 
				date_gpx, 
				audio_dir, 
				camera_dir, 
				gpx_dir, 
				finish, 
				traitement_en_cours) values ('%s', '%s', '%s', '%s', '%s', %s, %s, '%s', '%s', '%s', '%s', '%s', 0, 0);\n''' % (
				i['collection']['dr_dir_name'],
				i['collection']['road_name'],
				datetime.datetime.strptime(i['collection']['road_date'], "%d-%m-%Y").isoformat(),
				j[0][0],
				j[0][1],
				j[1][0].x,
				j[1][0].y,
				j[1][1],
				j[1][2].isoformat(),
				i['collection']['audio_dir'],
				i['collection']['camera_dir'],
				i['collection']['gpx_dir']))
	result_file.close()