/******************************************************************************
 * Some const
 ******************************************************************************/
var KEY_A     = 65;
var KEY_Z     = 90;
var KEY_Q     = 81;
var KEY_S     = 83;
var KEY_D     = 68;
var KEY_LEFT  = 37;
var KEY_UP    = 38;
var KEY_RIGHT = 39;
var KEY_DOWN  = 40;

var POST_ACTION         = 'POST'    ;
var OR_DATA_KEY         = 'data'    ;
var OR_SUCCESS_FN_KEY   = 'success' ;
var OR_FAILURE_FN_KEY   = 'failure' ;
var OR_MALFORM_FN_KEY   = 'malform' ;
var TYPEOF_FUNCTION_OBJ = 'function';

var AJAX_RESPONSE_STATUS_KEY  = "status" ;
var AJAX_RESPONSE_MESSAGE_KEY = "message";

var MALFORMED_RESPONSE_EM      = 'Malformed response, operation likely has failed !!';
var AJAX_TRANSFERT_FAILER_EM   = 'Ajax transfert operation failed!!! (@,@)"';
var DEFAULT_AJAX_ERROR_HANDLER = function(){ dialog_operation_failed(AJAX_TRANSFERT_FAILER_EM); };

/******************************************************************************
 * Generic information message dialog wrapper
 ******************************************************************************/
$(function(){
    $("#info_dialog").dialog("destroy");
    $("#info_dialog").dialog({
        resizable : false,
        height    : 300,
        width     : 400,
        modal     : true,
        autoOpen  : false,
        buttons   : { "OK": function(){ $(this).dialog("close"); }}
})});

function dialog_operation_information(message){
    $("#info_dialog #information_message").html(message);
    $("#info_dialog").dialog("open");
}

/******************************************************************************
 * Generic error message dialog wrapper
 ******************************************************************************/
$(function(){
    $("#error_dialog").dialog("destroy");
    $("#error_dialog").dialog({
        resizable : false,
        height    : 300,
        width     : 400,
        modal     : true,
        autoOpen  : false,
        buttons   : { "OK": function(){ $(this).dialog("close"); }}
})});

function dialog_operation_failed(error){
    $("#error_dialog #error_message")[0].innerHTML = error;
    $("#error_dialog").dialog("open");
}

/******************************************************************************
 * Customization of the ajaxSend function
 *    Updating the status buttons according to the s_states array value and
 *    current image index
 ******************************************************************************/
$(document).ajaxSend(function(event, xhr, settings) {
    /******************************************************************************
     * Get cookie value by name
     ******************************************************************************/
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
        }}}
        return cookieValue;
    }

    /******************************************************************************
     * Identify the request origin
     ******************************************************************************/
    function sameOrigin(url) {
        var host      = document.location.host;
        var protocol  = document.location.protocol;
        var sr_origin = '//' + host;
        var origin    = protocol + sr_origin;
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    /******************************************************************************
     * Test if the send method is a safe one (all except post/put/delete)
     ******************************************************************************/
    function safeMethod(method) { return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));}

    /******************************************************************************
     * Add the X-CSRFToken custom header with value from the cookie
     ******************************************************************************/
    if (!safeMethod(settings.type) && sameOrigin(settings.url))
        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
});

/******************************************************************************
 * Draw progression pie
 ******************************************************************************/
function draw_progress(percent){
    $('div.progress').html('<div class="percent"></div><div id="slice"'+(percent > 50?' class="gt50"':'')+'><div class="pie"></div>'+(percent > 50?'<div class="pie fill"></div>':'')+'</div>');
    var deg = 360/100*percent;
    $('#slice .pie').css({
        'transform'        :'rotate('+deg+'deg)',
        '-o-transform'     :'rotate('+deg+'deg)',
        '-moz-transform'   :'rotate('+deg+'deg)',
        '-webkit-transform':'rotate('+deg+'deg)',
    });
    $('.percent').html(Math.round(percent)+'%');
}

/******************************************************************************
 * Initial ajax loading animation
 ******************************************************************************/    
$(document).ajaxStart(function() { $('#wait').show(); }).ajaxComplete(function() { $('#wait').hide(); });

/******************************************************************************
 * Page redirection
 ******************************************************************************/    
function redirect_to(url){ window.location = url; }

/******************************************************************************
 * Ajax status reporter
 ******************************************************************************/    
function operation_report(obj){
    if(OR_DATA_KEY in obj) {
        var result = eval('('+obj[OR_DATA_KEY]+')');
        if(AJAX_RESPONSE_STATUS_KEY in result) {
            if(result[AJAX_RESPONSE_STATUS_KEY] == true) {
                dialog_operation_information(result[AJAX_RESPONSE_MESSAGE_KEY]);
                if(OR_SUCCESS_FN_KEY in obj && typeof(obj[OR_SUCCESS_FN_KEY]) == TYPEOF_FUNCTION_OBJ) 
                    obj[OR_SUCCESS_FN_KEY](result);
            } else {
                dialog_operation_failed(result[AJAX_RESPONSE_MESSAGE_KEY])
                if(OR_FAILURE_FN_KEY in obj && typeof(obj[OR_FAILURE_FN_KEY]) == TYPEOF_FUNCTION_OBJ) 
                    obj[OR_FAILURE_FN_KEY](result);
        }} else {
            dialog_operation_failed(MALFORMED_RESPONSE_EM);
            if(OR_MALFORM_FN_KEY in obj && typeof(obj[OR_MALFORM_FN_KEY]) == TYPEOF_FUNCTION_OBJ)
               obj[OR_MALFORM_FN_KEY](result);
}}}