var image_index      = 0;
var traversal_status = false;

var images    = new Array();
var angles    = new Array();
var s_angles  = new Array();
var s_states  = new Array();
var s_visited = new Array();

/******************************************************************************
 * Some const
 ******************************************************************************/
var VALID_STATE    = 1;
var INVALID_STATE  = 2;
var AMBIGUES_STATE = 3;

var VISITED        = true;
var NOT_VISITED    = false;

var INITIAL_ANGLE  = 0;

var IMAGE_CANVAS_WIDTH  = 560;
var IMAGE_CANVAS_HEIGHT = 480;

var SLIDE_SHOW_IMAGE_X      =  45;
var SLIDE_SHOW_IMAGE_Y      =  40;
var SLIDE_SHOW_IMAGE_WIDTH  = 480;
var SLIDE_SHOW_IMAGE_HEIGHT = 380;

var TAINT_KEY                = 'taint';

var GALLERIES_URL            = '/tajhiz/galleries/'           ;
var QUALIFICATION_URL        = '/tajhiz/qualification/'       ;
var SAVE_QUALIFICATION_URL   = '/tajhiz/save_qualifications/' ;
var RELEASE_GALLERY_LOCK_URL = '/tajhiz/release_gallery_lock/';

var STATES_BUTTONS_MAPPING  = new Array();
STATES_BUTTONS_MAPPING [VALID_STATE   ] = "valid"  ;
STATES_BUTTONS_MAPPING [INVALID_STATE ] = "invalid";
STATES_BUTTONS_MAPPING [AMBIGUES_STATE] = "anbigus";

var TRAVERSAL_COMPLETE_MESSAGE   = 'Felicitations,<br/> En parcourant cette image toute auront été visitées (au moins une fois), <br/>Il est recommendé d\'enregistrer votre qualification';

var GALLERY_RELEASE_FAILED_EM    = 'Erreur lors du deverouillage de la gallery';
var NO_IMAGE_FOUND_IN_GALLERY_EM = 'Désolé,<br/>mais la gallerie actuelle ne semble pas contenir des images, vous allez être redirigé vers la page sélection des galleries.';

/******************************************************************************
 * Updating the image traversal thumbnails
 *    Updating the thumbnail Css class
 ******************************************************************************/
function update_traversal(v){
	v.visite(image_index);
	$(".gallery li[title='"+(image_index+1)+"'] .thumbnail_visited_status").addClass('thumbnail_visited');
}

/******************************************************************************
 * Updating the image visited status using signal/slot technique:
 *    s_visited array is updated at the image index
 *    check for all_visited state is performed
 ******************************************************************************/
function Visitor () {
	this.visited_index = function (index ) {}
	this.visite        = function (index ) { $.jSignal.emit(this.visited_index, index ); }
	this.all_images_visited = function (status) { 
		traversal_status = status;
		if(images_count > 0)
			dialog_operation_information (TRAVERSAL_COMPLETE_MESSAGE);
		else {
			$.ajax({
				url     : RELEASE_GALLERY_LOCK_URL+gallery_name+'/',
				type    : POST_ACTION,
				error   : DEFAULT_AJAX_ERROR_HANDLER,
				success : function(data){
					operation_report_config = {}
					operation_report_config[OR_DATA_KEY      ] = data;
					operation_report_config[OR_SUCCESS_FN_KEY] = function(){ redirect_to(QUALIFICATION_URL); }
					operation_report(operation_report_config);
}})}}}

function Iterator() {
	this.all_visited   = function (status) {                                                                                      }
	this.visited_all   = function (status) { $.jSignal.emit(this.all_visited, status );                                           }
	this.reset_visited = function (      ) { for(i=0;i<s_visited.length;i++) s_visited[i] = NOT_VISITED; traversal_status = false;}
	this.visited_image = function (index ) {
		if(s_visited[index]) return;
		s_visited[index] = VISITED;
		if(!traversal_status) {
			visited_result = 1;
			total_visited = 0
			for(i=0;i<s_visited.length;i++){
				visited_result &= s_visited[i];
				total_visited  += s_visited[i] ? 1 : 0;
			}
			draw_progress((total_visited * 100)/s_visited.length);
			if (!visited_result) return;
			this.visited_all(true);
}}}

var visitor  = new Visitor ();
var iterator = new Iterator();

/******************************************************************************
 * Updating the image rotation angle:
 *    Computing the angle to show in the #rotation_value
 *    Storing the the specific image rotation value in the s_angles array
 *    Updating the #rotation_value and alt description
 ******************************************************************************/
function update_angle(){
	s_angles[image_index] = angles[image_index] % 360;
	var rotation_img = "";
	switch(s_angles[image_index]){
		case 0    : rotation_img = "transform_rotate_0.png"   ; break;
		case 90   : rotation_img = "transform_rotate_90.png"  ; break;
		case 180  : rotation_img = "transform_rotate_180.png" ; break;
		case 270  : rotation_img = "transform_rotate_270.png" ; break;
		case -90  : rotation_img = "transform_rotate_270.png" ; break;
		case -180 : rotation_img = "transform_rotate_180.png" ; break;
		case -270 : rotation_img = "transform_rotate_90.png"  ; break;
	}
	$("#rotation_value")[0].src  = media_prefix + "Img/" + rotation_img;
	$("#rotation_value")[0].alt  = s_angles[image_index]+ "°";
}

/******************************************************************************
 * Updating the image status:
 *    Updating the status buttons according to the s_states array value and
 *    current image index
 ******************************************************************************/
function update_states(){
	var current_state = s_states[image_index];
	$("#"+STATES_BUTTONS_MAPPING[current_state]).addClass('button_active').siblings().removeClass('button_active');
}

/******************************************************************************
 * Initialization function
 ******************************************************************************/
$(document).ready( function() {
	/******************************************************************************
	 * Setting up the visitor/iterator objects
	 ******************************************************************************/
	$.jSignal.connect(visitor,  visitor.visited_index, iterator, iterator.visited_image    );
	$.jSignal.connect(iterator, iterator.all_visited , visitor , visitor.all_images_visited);

	/******************************************************************************
	 * Initialization of the custom scrollbar
	 ******************************************************************************/
	$("#mcs_container").mCustomScrollbar("vertical",800,"easeOutCirc",1.05,"auto","yes","yes",5);

	/******************************************************************************
	 * Slideshow initialization
	 ******************************************************************************/
	for(i=0;i<images_count;i++){
		current_holder = $("#holder_"+(i+1))[0];
		current_canvas = Raphael(current_holder, IMAGE_CANVAS_WIDTH, IMAGE_CANVAS_HEIGHT);
		current_image  = current_canvas.image(image_sources[i], SLIDE_SHOW_IMAGE_X, SLIDE_SHOW_IMAGE_Y, SLIDE_SHOW_IMAGE_WIDTH, SLIDE_SHOW_IMAGE_HEIGHT);
		current_tuple  = new Array(current_canvas, current_image);
		
		images   .push(current_tuple);
		angles   .push(INITIAL_ANGLE);
		s_angles .push(INITIAL_ANGLE);
		s_states .push(VALID_STATE  );
		s_visited.push(NOT_VISITED  );
	}

	draw_progress(0);
	update_traversal(visitor);
	$('.simpleSlide-slide').not(":eq(0)").hide();

	/******************************************************************************
	 * Initialization of the toggle state buttons with default set to valid state
	 ******************************************************************************/
	update_states();

	/******************************************************************************
	 * Hook the click action on the toggle group state to switch image state
	 ******************************************************************************/
	$('.contrls_toggle_group_1').live('click', function(){
		s_states[image_index] = $(this).val();
		$(this).addClass('button_active').siblings().removeClass('button_active');
		return false;
	});

	/******************************************************************************
	 * Hook the click action on the gallery thumbnails to switch the current image
	 ******************************************************************************/
	$('.gallery li').live('click', function(){
		$(".simpleSlide-slide").eq(image_index ).fadeOut('slow');
		image_index = $(this).attr('title') - 1;
		update_angle ();
		update_states();
		$(".simpleSlide-slide").eq(image_index ).fadeIn('slow');
	});

	/******************************************************************************
	 * Slide show transition button actions and arrow keys
	 ******************************************************************************/
	$(document).keydown(function(e){
		switch(e.keyCode){
			case KEY_UP    : $('.top-button'          ).click(); return false;
			case KEY_DOWN  : $('.down-button'         ).click(); return false;
			case KEY_LEFT  : $('.left-button'         ).click(); return false;
			case KEY_RIGHT : $('.right-button'        ).click(); return false;
			case KEY_Q     : $('#valid'               ).click(); return false;
			case KEY_S     : $('#invalid'             ).click(); return false;
			case KEY_D     : $('#anbigus'             ).click(); return false;
			case KEY_Z     : $('#rotate_clockwise'    ).click(); return false;
			case KEY_A     : $('#rotate_anticlockwise').click(); return false;
		}
	});

	$('.top-button').live( 'click', function() {
		$(".simpleSlide-slide").eq(image_index ).fadeOut('slow');
		image_index = 0
		update_angle ();
		update_states();
		update_traversal(visitor);
		$(".simpleSlide-slide").eq(image_index ).fadeIn('slow');     
	});

	$('.down-button').live( 'click', function() {
		$(".simpleSlide-slide").eq(image_index ).fadeOut('slow');
		image_index = images.length-1;
		update_angle ();
		update_states();
		update_traversal(visitor);
		$(".simpleSlide-slide").eq(image_index ).fadeIn('slow');     
	});

	$('.right-button').live( 'click', function() { 
		$(".simpleSlide-slide").eq(image_index ).fadeOut('slow');
		image_index = ++image_index > images_count-1 ? 0 : image_index;
		update_angle ();
		update_states();
		update_traversal(visitor);
		$(".simpleSlide-slide").eq(image_index ).fadeIn('slow');     
	});

	$('.left-button' ).live( 'click', function() { 
		$(".simpleSlide-slide").eq(image_index ).fadeOut('slow');
		image_index = --image_index < 0 ? images_count-1 : image_index;
		update_angle ();
		update_states();
		update_traversal(visitor);
		$(".simpleSlide-slide").eq(image_index ).fadeIn('slow');
	});

	/******************************************************************************
	 * Image rotation actions
	 ******************************************************************************/
	$("#rotate_anticlockwise").click(function () {
		angles[image_index] -= 90;
		images[image_index][1].stop().animate({transform: "r" + angles[image_index]}, 1500, "<>");
		update_angle();
		return false;
	});

	$("#rotate_clockwise").click(function () {
		angles[image_index] += 90;
		images[image_index][1].animate({transform: "r" + angles[image_index]}, 1500, "<>");
		update_angle();
		return false;
	});

	/******************************************************************************
	 * Save action
	 ******************************************************************************/
	$("#save").click(function(){
		$.ajax({
		   url     : SAVE_QUALIFICATION_URL,
		   type    : POST_ACTION,
		   data    : {
				"angles" : s_angles    , 
				"states" : s_states    ,
				"images" : image_titles,
		   },
		   error   : DEFAULT_AJAX_ERROR_HANDLER,
		   success : function(data){
				operation_report_config = {}
				operation_report_config[OR_DATA_KEY] = data;
				operation_report(operation_report_config);
		}});
	   return false; 
	});

	/******************************************************************************
	 * Reset action
	 ******************************************************************************/
	$("#reset").click(function(){
		images[image_index][1].stop().animate({transform: "r0"}, 1500, "<>");
		for(i=0;i<images.length;i++){
			images[i][1].rotate(-angles[image_index]);
			angles[i] = 0;
			update_angle();
		}
		draw_progress(100/images.length);
		iterator.reset_visited()
		$(".gallery li .thumbnail_visited_status").removeClass('thumbnail_visited');
		update_traversal(visitor);
		return false; 
	});

	/******************************************************************************
	 * Release lock action
	 ******************************************************************************/
	$("#gallery_release_dialog").dialog("destroy");
	$("#gallery_release_dialog").dialog({
		resizable : false,
		height    : 300,
		width     : 400,
		modal     : true,
		autoOpen  : false, 
		buttons   : {
			"Oui": function(){
				$.ajax({
					url     : RELEASE_GALLERY_LOCK_URL+gallery_name+"/",
					type    : POST_ACTION,
					error   : DEFAULT_AJAX_ERROR_HANDLER,
					success : function(data){
						operation_report_config = {}
						operation_report_config[OR_DATA_KEY      ] = data;
						operation_report_config[OR_SUCCESS_FN_KEY] = function(){ redirect_to(GALLERIES_URL); }
						operation_report(operation_report_config);       
			}})},
			"Non": function(){ $(this).dialog("close") },
			"Oui, mais marquer 'en cour'" : function(){
				$.ajax({
					url     : RELEASE_GALLERY_LOCK_URL+gallery_name+"/",
					type    : POST_ACTION,
					data    : { 'taint' : true },
					error   : DEFAULT_AJAX_ERROR_HANDLER,
					success : function(data){
						operation_report_config = {}
						operation_report_config[OR_DATA_KEY      ] = data;
						operation_report_config[OR_SUCCESS_FN_KEY] = function(){ redirect_to(GALLERIES_URL); }
						operation_report(operation_report_config);
				}});
				$(this).dialog("close");
	}}});

	$("#release").live('click', function(){
		$("#gallery_release_dialog").dialog('open');
		return false;
	});    
});

/******************************************************************************
 * Fix for the > 1000 length on the custom slider
 ******************************************************************************/
$.fx.prototype.cur = function(){
	if ( this.elem[this.prop] != null && (!this.elem.style || this.elem.style[this.prop] == null) ) {
	  return this.elem[ this.prop ];
	}
	var r = parseFloat( jQuery.css( this.elem, this.prop ));
	return typeof r == 'undefined' ? 0 : r;
}
