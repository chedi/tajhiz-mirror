import logging
from django.conf               import settings
from django.http               import HttpResponseRedirect
from django.template           import RequestContext
from django.shortcuts          import render_to_response
from django.contrib.auth.views import redirect_to_login
from django.http               import HttpResponse

from tajhiz.saisie.models import *
from django.contrib.auth import logout

from django.utils                   import simplejson
from django.views.decorators.csrf   import csrf_protect
from django.views.decorators.cache  import never_cache
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required

from django.db import transaction

from tajhiz.saisie.customDecorator import *

from photologue.models import *

from chartit import DataPool, Chart
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

TAINT_KEY           = 'taint'
RESULT_MESSAGE_KEY  = 'message'
RESULT_LOCKABLE_KEY = 'lockable'

logger = logging.getLogger(__name__)

def welcome(request):
    if 'django.contrib.gis' in settings.INSTALLED_APPS:
        return render_to_response('welcome.html', {})
    else:
        return render_to_response('welcome_error.html', {})

def saisie_logout(request, logout_template='registration/logged_out.html'):
    logout(request)
    return render_to_response(logout_template, { 'media_prefix' : settings.MEDIA_URL })

def i_am_not_your_fadher_error():
    error_message = "You are not supposed to be here....and I'm not your father!!!"
    resp = render_to_response('501.html', { 'custom_error_message' : error_message }, context_instance=RequestContext(request))
    resp.status_code = 501
    return resp

def gallery_lockable_by_user(gallery_title, user):
    result           = {}
    gallery          = GalleryUserOwnership.objects.filter(gallery__title=gallery_title)
    galleries_locked = GalleryUserOwnership.objects.filter(owner=user,locked=True)
    galleries_locked = list(set(galleries_locked) - set(gallery))

    if gallery:
        gal = gallery[0]
        if not gal.locked:
            if galleries_locked and len(galleries_locked) > 0:
                result = {
                    RESULT_LOCKABLE_KEY : False, 
                    RESULT_MESSAGE_KEY : "You exceeded the maximum number of locked galleries, you can fool some people some time but you can't fool all the people all the time!!!" }
            else:
                result = {
                    RESULT_LOCKABLE_KEY : True,
                    RESULT_MESSAGE_KEY  : "You can acquire this gallery lock",}
        elif gal.owner == user:
            result = {
                RESULT_LOCKABLE_KEY : True,
                RESULT_MESSAGE_KEY  : "You can always acquire your locked galleries, and eat your dog food",}
        else:
            result = {
                RESULT_LOCKABLE_KEY : False,
                RESULT_MESSAGE_KEY  : "You cannot acquire other users locks, cheat code 'all your bases are belong to us' disabled",}
    else:
        result = {
            RESULT_LOCKABLE_KEY : False,
            RESULT_MESSAGE_KEY  : "Requested gallery not found, you got to use you medication or stop using it",}
    return result

@csrf_protect
@login_required
@never_cache
@permission_required_with_403('saisie.can_rotate')
@permission_required_with_403('saisie.can_qualify')
@log_user_action_start(activity_name='Qualification photo')
def qualification_photo_form(request, gallery_title, fail_silently=False):
    template_name = 'saisie/qualification_photos.html'
    if request.method == 'GET':
        result = gallery_lockable_by_user(gallery_title, request.user)
        if result[RESULT_LOCKABLE_KEY]:
            gallery      = GalleryUserOwnership.objects.filter(gallery__title=gallery_title)
            gal          = gallery[0]
            gal.owner    = request.user
            gal.locked   = True
            gal.progress = 1
            gal.save()
            return render_to_response(template_name, { 'gallery' : gal }, context_instance=RequestContext(request))
        else:
            resp = render_to_response('501.html', { 'custom_error_message' : result[RESULT_MESSAGE_KEY] }, context_instance=RequestContext(request))
            resp.status_code = 501
            return resp
            
@csrf_protect
@login_required
@never_cache
@transaction.commit_manually
@permission_required_with_403('saisie.can_rotate')
@permission_required_with_403('saisie.can_qualify')
@log_user_action_end(activity_name='Qualification photo')
def save_qualifications(request):
    if request.method == 'POST':
        index  = 0
        images = request.POST.getlist('images[]')
        states = request.POST.getlist('states[]')
        angles = request.POST.getlist('angles[]')

        error         = ""
        response_dict = {}

        ROTATE_90  = PhotoEffect.objects.filter(name="rotate_90" )
        ROTATE_180 = PhotoEffect.objects.filter(name="rotate_180")
        ROTATE_270 = PhotoEffect.objects.filter(name="rotate_270")

        if not ROTATE_90 or not ROTATE_180 or not ROTATE_270:
            error = 'Some rotation effects are missing, please check the database'
        else:
            ROTATE_90  = ROTATE_90  [0]
            ROTATE_180 = ROTATE_180 [0]
            ROTATE_270 = ROTATE_270 [0]

            for image in images:
                media = Media.objects.filter(image__title=image)
                if media:
                    media = media[0]
                    if   angles[index] ==   '0': pass
                    elif angles[index] ==  '90' or angles[index] == '-270':
                        media.image.effect = ROTATE_90
                        media.image.save()
                    elif angles[index] == '180' or angles[index] == '-180':
                        media.image.effect = ROTATE_180
                        media.image.save()
                    elif angles[index] == '270' or angles[index] == '-90':
                        media.image.effect = ROTATE_270
                        media.image.save()
                    else:
                        error = 'invalide rotation value: '+ angles[index]
                        break
                    media.image_valide = states[index]
                    media.save()
                else:
                    error = "(media not found) : request image %s not found" % image
                    break
                index += 1
        if error != "":
            transaction.rollback()
            response_dict = {'status' : False, 'message' : error}
        else:
            transaction.commit()
            response_dict = {'status' : True, 'message' : 'Gallery saved successfuly !!'}
        return HttpResponse(simplejson.dumps(response_dict), content_type = 'application/javascript; charset=utf8')
    else:
        return i_am_not_your_fadher_error()

@never_cache
@csrf_protect
@login_required
def gallery_browser(request):
    if request.method == 'GET':
        drs_labels = DirectionRegionale.objects.values('label')
        galleries_ownership = GalleryUserOwnership.objects.select_related()
        return render_to_response('saisie/galleries_browser.html', {'galleries':galleries_ownership, 'drs' : drs_labels }, context_instance=RequestContext(request))
    else:
        return i_am_not_your_fadher_error()

@csrf_protect
@login_required
@log_user_action_start(activity_name='Release gallery lock')
@permission_required_with_403('saisie.can_unlock')
def release_gallery_lock(request, gallery_title, taint_gallery=False):
    if request.method == 'POST':
        gallery = GalleryUserOwnership.objects.filter(owner=request.user,locked=True,gallery__title=gallery_title)
        response_dict = {}
        if TAINT_KEY in request.POST:
            taint_gallery = bool(request.POST[TAINT_KEY])

        if not gallery:
            response_dict = {'status' : False, 'message' : 'Gallery '+gallery_title+' not found!!'}
        else:
            gal = gallery[0]
            gal.locked = False
            gal.progress = 1 if taint_gallery else 2
            gal.save()
            response_dict = {'status' : True, 'message' : 'Gallery '+gallery_title+' released successfuly'}
        return HttpResponse(simplejson.dumps(response_dict), content_type = 'application/javascript; charset=utf8')
    else:
        return i_am_not_your_fadher_error()

@csrf_protect
@login_required
@log_user_action_start(activity_name='Release gallery lock')
@permission_required_with_403('saisie.can_unlock')
def get_gallery_lock(request, gallery_title):
    if request.method == 'POST':
        result = gallery_lockable_by_user(gallery_title, request.user)
        response_dict = {'status' : result[RESULT_LOCKABLE_KEY], 'message' : result[RESULT_MESSAGE_KEY]}
        return HttpResponse(simplejson.dumps(response_dict), content_type = 'application/javascript; charset=utf8')
    else:
        return i_am_not_your_fadher_error()    