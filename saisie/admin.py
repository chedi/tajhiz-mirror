from tajhiz.saisie.models      import *
from tajhiz.saisie.customAdmin import *
from django.contrib            import admin

admin.site.register( Voie                        ,VoieAdmin               )
admin.site.register( Media                       ,MediaAdmin              )
admin.site.register( Noeud                       ,NoeudAdmin              )
admin.site.register( Support                      )
admin.site.register( Activite                     )
admin.site.register( Tracking                    ,TrackingAdmin           )
admin.site.register( Collecte                     )
admin.site.register( Telephone                    )
admin.site.register( Panonceau                    )
admin.site.register( Fabriquant                   )
admin.site.register( Utilisateur                  )
admin.site.register( TextePanneau                 )
admin.site.register( SignalRoutier               ,SignalRoutierAdmin      )
admin.site.register( PanneauPolice                )
admin.site.register( PanneauAnomalie              )
admin.site.register( PanneauRegistre              )
admin.site.register( CategoriePanneau             )
admin.site.register( PanneauObservation           )
admin.site.register( DirectionRegionale          ,DirectionRegionaleAdmin )
admin.site.register( IndicationDirection          )
admin.site.register( PanneauDirectionnel          )
admin.site.register( PanneauDiagramatique         )
admin.site.register( GalleryUserOwnership         )
admin.site.register( PanneauPoliceCodeInfo        )
admin.site.register( SignalRoutierAnomalies       )
admin.site.register( PanneauRegistreCodeInfo      )
admin.site.register( SignalRoutierObservation     )
admin.site.register( PanneauDirectionnelCodeInfo  )
admin.site.register( PanneauDiagramatiqueCodeInfo )

